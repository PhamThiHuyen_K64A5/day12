<?php include 'connectionPDO.php';
session_start();
$faculty = array('' => '', 'MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
$gender = array(0 => 'Nữ', 1 => 'Nam');
if (isset($_POST['submit'])) {
    $sql = "SELECT COUNT(*) FROM Student";
    $count = $con->query($sql)->fetchColumn();
    $timestamp = DateTime::createFromFormat("d/m/Y", $_SESSION["dateBirth"]);
    $datetime = $timestamp->format("Y-m-d H:i:s");
    $sql = "INSERT INTO Student (id, name, gender, faculty, birthday, address, avatar) 
                VALUES (" . ($count + 1) . ", '" . $_SESSION["name"] . "', " . $_SESSION["gender"] . ", '" . $_SESSION["faculty"] . "', '" . $datetime . "', '" . $_SESSION["address"] . "', '" . $_SESSION["image"] . "')";
    $con->query($sql);
    header('location: complete_regist.php');
}
?>

<!DOCTYPE html>
<html lang='en'>

<head>
    <meta charset='UTF-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <title>Confirm</title>
    <link rel='stylesheet' type='text/css' href='confirm.css'>
</head>

<body>
    <form method='post'>
        <table>
            <tr>
                <td>
                    <div class="green_background"><label> Họ và tên </label></div>
                </td>
                <td>
                    <lable><?php echo $_SESSION["name"]; ?></lable>
                </td>
            </tr>

            <tr>
                <td>
                    <div class="green_background"><label> Giới tính </label></div>
                </td>
                <td>
                    <lable><?php echo $gender[$_SESSION["gender"]]; ?></lable>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="green_background"><label> Phân khoa </label></div>
                </td>
                <td>
                    <lable><?php echo $faculty[$_SESSION["faculty"]]; ?></lable>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="green_background"><label> Ngày sinh </label></div>
                </td>
                <td>
                    <lable><?php echo $_SESSION["dateBirth"]; ?></lable>
                </td>
            </tr>

            <tr>
                <td>
                    <div class="green_background"><label> Địa chỉ </label></div>
                </td>
                <td>
                    <lable><?php echo $_SESSION["address"]; ?></lable>
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <div class="green_background"><label> Hình ảnh </label></div>
                </td>
                <td>
                    <img src='<?php echo $_SESSION["image"] ?>' width='90px' height="60px" />
                </td>
            </tr>
        </table>
        <input type='submit' name='submit' value='Xác nhận' class='submit'>
    </form>

</body>

</html>